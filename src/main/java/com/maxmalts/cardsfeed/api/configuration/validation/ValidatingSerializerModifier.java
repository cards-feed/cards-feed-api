package com.maxmalts.cardsfeed.api.configuration.validation;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

public class ValidatingSerializerModifier extends BeanSerializerModifier {
    @Override
    public JsonSerializer<?> modifySerializer(
        SerializationConfig config,
        BeanDescription beanDesc,
        JsonSerializer<?> serializer
    ) {
        return new ValidatingSerializer((JsonSerializer<Object>) serializer);
    }
}