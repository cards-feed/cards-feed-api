package com.maxmalts.cardsfeed.api.configuration.settings;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("settings.error")
public class ErrorSettings {

    private ErrorTypeSettings business = new ErrorTypeSettings(422, "Error");
    private ErrorTypeSettings internal = new ErrorTypeSettings(500, "Internal Server Error");
    private ErrorTypeSettings unknown = new ErrorTypeSettings(500, "Unknown Error");

    @Getter
    @Setter
    @AllArgsConstructor
    public static class ErrorTypeSettings {
        private int statusCode;
        private String defaultUserMessage;
    }
}
