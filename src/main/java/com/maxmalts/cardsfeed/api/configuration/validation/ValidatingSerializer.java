package com.maxmalts.cardsfeed.api.configuration.validation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.maxmalts.cardsfeed.api.error.exception.SerializationValidationException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

import java.io.IOException;
import java.util.Set;

class ValidatingSerializer extends JsonSerializer<Object> {
    private final JsonSerializer<Object> defaultSerializer;
    private final Validator validator;

    ValidatingSerializer(final JsonSerializer<Object> defaultSerializer) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        this.validator = factory.getValidator();
        this.defaultSerializer = defaultSerializer;
    }

    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        Set<ConstraintViolation<Object>> violations = validator.validate(value);
        if (!violations.isEmpty()) {
            ConstraintViolation<Object> firstViolation = violations.iterator().next();
            var errorMessage = "Constraint violation on field '%s'. Reason: %s. Actual field value: %s".formatted(
                firstViolation.getPropertyPath(),
                firstViolation.getMessage(),
                firstViolation.getInvalidValue()
            );
            throw new SerializationValidationException(errorMessage);
        }

        defaultSerializer.serialize(value, gen, provider);
    }
}