package com.maxmalts.cardsfeed.api.configuration.logging;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class MethodLoggingAspect {

    private final ObjectMapper objectMapper;

    @Around(value = "@annotation(com.maxmalts.cardsfeed.api.annotation.Log)")
    public Object beforeMethodExecutionAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        var codeSignature = (CodeSignature) proceedingJoinPoint.getSignature();
        var methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

        var fullMethodName = "%s.%s".formatted(
                methodSignature.getMethod().getDeclaringClass().getTypeName(),
                methodSignature.getMethod().getName()
        );

        // IN
        var messageBuilderIn = new StringBuilder("> " + fullMethodName);

        assert codeSignature.getParameterNames().length == proceedingJoinPoint.getArgs().length;
        var argsCount = codeSignature.getParameterNames().length;
        if (argsCount > 0) {
            messageBuilderIn.append(" arguments:");
            for (int i = 0; i < argsCount; ++i) {
                var paramName = codeSignature.getParameterNames()[i];
                var paramValue = proceedingJoinPoint.getArgs()[i];
                messageBuilderIn.append(" %s: %s;".formatted(paramName, serialize(paramValue)));
            }
        }
        log.debug(messageBuilderIn.toString());

        // ERROR
        Object result;
        try {
            result = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            log.debug("! %s threw %s: %s".formatted(
                    fullMethodName,
                    e.getClass().getTypeName(),
                    e.getMessage()
            ));
            throw e;
        }

        // OUT
        var messageBuilderOut = new StringBuilder("< " + fullMethodName);
        if (methodSignature.getMethod().getReturnType() != Void.TYPE) {
            messageBuilderOut.append(" result: %s".formatted(serialize(result)));
        }
        log.debug(messageBuilderOut.toString());

        return result;
    }

    private String serialize(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            log.warn("Failed to serialize object while logging method execution", e);
            return "";
        }
//        return obj.toString().replaceAll("\n *", "; ");
    }
}
