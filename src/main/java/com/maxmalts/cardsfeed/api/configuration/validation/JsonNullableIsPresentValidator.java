package com.maxmalts.cardsfeed.api.configuration.validation;

import com.maxmalts.cardsfeed.api.annotation.JsonNullableIsPresent;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.openapitools.jackson.nullable.JsonNullable;

public class JsonNullableIsPresentValidator implements ConstraintValidator<JsonNullableIsPresent, JsonNullable<?>> {
    @Override
    public boolean isValid(JsonNullable<?> value, ConstraintValidatorContext context) {
        return value != null && value.isPresent();
    }
}
