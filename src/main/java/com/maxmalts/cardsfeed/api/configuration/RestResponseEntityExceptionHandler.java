package com.maxmalts.cardsfeed.api.configuration;

import com.maxmalts.cardsfeed.api.configuration.settings.ErrorSettings;
import com.maxmalts.cardsfeed.api.error.BusinessException;
import com.maxmalts.cardsfeed.api.error.InternalException;
import com.maxmalts.cardsfeed.api.openapi.model.ErrorResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class RestResponseEntityExceptionHandler {

    private final ErrorSettings errorSettings;

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleInternalException(BusinessException ex) {
        log.error("Business exception occurred", ex);

        var errorResponse = new ErrorResponse(
            "Error",
            ex.getUserMessage() == null ? errorSettings.getBusiness().getDefaultUserMessage() : ex.getUserMessage()
        );

        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(errorSettings.getBusiness().getStatusCode()));
    }

    @ExceptionHandler(InternalException.class)
    public ResponseEntity<Object> handleInternalException(InternalException ex) {
        log.error("Internal exception occurred", ex);

        var errorResponse = new ErrorResponse(
            "InternalServerError",
            ex.getUserMessage() != null ? ex.getUserMessage() : errorSettings.getInternal().getDefaultUserMessage()
        );

        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(errorSettings.getInternal().getStatusCode()));
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleUnknownException(Exception ex) {
        log.error("Unknown exception occurred", ex);

        var errorResponse = new ErrorResponse(
            "InternalServerError",
            errorSettings.getUnknown().getDefaultUserMessage()
        );

        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(errorSettings.getUnknown().getStatusCode()));
    }
}
