package com.maxmalts.cardsfeed.api.annotation;

import com.maxmalts.cardsfeed.api.configuration.validation.JsonNullableIsPresentValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = JsonNullableIsPresentValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonNullableIsPresent {
    String message() default "JsonNullable is not present";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
