package com.maxmalts.cardsfeed.api.controller;

import com.maxmalts.cardsfeed.api.annotation.Log;
import com.maxmalts.cardsfeed.api.openapi.controller.CommentApi;
import com.maxmalts.cardsfeed.api.openapi.model.AddCommentRequest;
import com.maxmalts.cardsfeed.api.openapi.model.GetCommentsResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class RestCommentController implements CommentApi {
    @Log
    @Override
    public ResponseEntity<Void> addComment(String xAppName, UUID cardId, AddCommentRequest addCommentRequest) {
        return ResponseEntity.ok().build();
    }

    @Log
    @Override
    public ResponseEntity<GetCommentsResponse> getComments(String xAppName, UUID cardId, Integer limit, UUID cursor) {
        return ResponseEntity.ok(new GetCommentsResponse(null, false, List.of()));
    }

    @Log
    @Override
    public ResponseEntity<Void> likeComment(String xAppName, UUID commentId) {
        return ResponseEntity.ok().build();
    }

    @Log
    @Override
    public ResponseEntity<Void> unlikeComment(String xAppName, UUID commentId) {
        return ResponseEntity.ok().build();
    }
}
