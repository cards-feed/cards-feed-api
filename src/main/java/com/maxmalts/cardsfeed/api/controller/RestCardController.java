package com.maxmalts.cardsfeed.api.controller;

import com.maxmalts.cardsfeed.api.annotation.Log;
import com.maxmalts.cardsfeed.api.openapi.controller.CardApi;
import com.maxmalts.cardsfeed.api.openapi.model.AddCardRequest;
import com.maxmalts.cardsfeed.api.openapi.model.Card;
import com.maxmalts.cardsfeed.api.openapi.model.GetCardsResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@RestController
public class RestCardController implements CardApi {

    @Log
    @Override
    public ResponseEntity<Void> addCard(String xAppName, AddCardRequest addCardRequest) {
        return ResponseEntity.ok().build();
    }

    @Log
    @Override
    public ResponseEntity<GetCardsResponse> getCards(String xAppName, Integer limit, UUID cursor) {
        return ResponseEntity.ok(new GetCardsResponse(null, false, List.of()));
    }

    @Log
    @Override
    public ResponseEntity<Card> getCard(String xAppName, UUID cardId) {
        return ResponseEntity.ok(new Card(cardId, "title", "author", OffsetDateTime.now(), 10, 5, "content"));
    }

    @Log
    @Override
    public ResponseEntity<Void> likeCard(String xAppName, UUID cardId) {
        return ResponseEntity.ok().build();
    }

    @Log
    @Override
    public ResponseEntity<Void> unlikeCard(String xAppName, UUID cardId) {
        return ResponseEntity.ok().build();
    }
}
