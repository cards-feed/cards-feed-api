package com.maxmalts.cardsfeed.api.error.exception;

public class SerializationValidationException extends RuntimeException {
    public SerializationValidationException() {
        super();
    }

    public SerializationValidationException(String message) {
        super(message);
    }

    public SerializationValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SerializationValidationException(Throwable cause) {
        super(cause);
    }

    public SerializationValidationException(
        String message,
        Throwable cause,
        boolean enableSuppression,
        boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
